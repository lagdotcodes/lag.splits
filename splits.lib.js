/* Helper Functions */
function fmtTime(s) {
	if (isNaN(s)) return '—';
	if (s < 60) return s.toFixed(2);
	var mins = Math.floor(s / 60);
	var secs = (s - mins * 60).toFixed(2);
	if (secs < 10) secs = '0' + secs;
	return mins + ':' + secs;
}

if (window.performance) getTime = function() { return window.performance.now() / 1000; };
else getTime = function() { return new Date().getTime() / 1000; };

function ajaxOp(op, data, cb) {
	data['op'] = op;
	$.getJSON('ajax.php', data, function(data, textStatus, jqXHR) {
		if (data.error) alert(data.error);
		else if (cb) cb(data);
	});
}
$(document).ajaxStart(function() { $.mobile.loading('show'); });
$(document).ajaxStop(function() { $.mobile.loading('hide'); });

function parseTime(v) {
	if (!isNaN(v)) return parseFloat(v);
	var i = v.indexOf(':');
	if (i == -1) return NaN;
	var mins = parseInt(v.substring(0, i));
	var secs = parseFloat(v.substring(i + 1));
	return mins * 60 + secs;
}

/* Splits */
function Splits(game, category) {
	this.ID = null;
	this.userID = null;
	this.game = game;
	this.category = category;
	this.splits = [];
	this.parent = null;
}
Splits.prototype.add = function(split) {
	split.parent = this;
	split.position = this.splits.length;
	if (!isNaN(split.best) && isNaN(split.bestSplit)) {
		split.bestSplit = this.splits.length == 0 ? split.best : split.best - this.splits[this.splits.length - 1].best;
		split.oldBestSplit = split.bestSplit;
	}
	this.splits.push(split);
};
Splits.prototype.reset = function() {
	this.splits.forEach(function(split) { split.reset(); });
};
Splits.prototype.trash = function() {
	this.splits.forEach(function(split) { split.trash(); });
};
Splits.prototype.show = function(e) {
	var t = $('<table class="mono" id="splitTable">');
	t.append($('<thead><th colspan="5" class="roboto">' + this.game + ' (' + this.category + ')</th></thead>'));
	var tb = $('<tbody>');
	tb.append('<tr class="roboto"><th>Split</th><th>Time</th><th>Best Run</th><th>Best Split</th></tr>');
	this.splits.forEach(function(split) { tb.append(split.show()); } );
	t.append(tb);
	e.empty();
	e.append(t);
};
Splits.prototype.edit = function() {
	this.splits.forEach(function(split) { split.edit(); });
};
Splits.prototype.remove = function(split) {
	this.splits.splice(split.position, 1);
	var i = 0;
	this.splits.forEach(function(x) { x.position = i++; });
	split.el.parent().remove();
};
Splits.prototype.toJSON = function() {
	var s = [];
	this.splits.forEach(function(split) { s.push(split.toJSON()); });
	return {game: this.game, category: this.category, splitData: s, ID: this.ID, userID: this.userID, splits: this.splits.length};
};
Splits.fromJSON = function(data) {
	var set = new Splits(data.game, data.category);
	set.ID = data.ID;
	set.userID = data.userID;
	data.splitData.forEach(function(split) { set.add(Split.fromJSON(split, set)); });
	return set;
};
Splits.prototype.saveBests = function() {
	this.splits.forEach(function(split) { split.saveBests(); });
};

/* Split */
var splitId = 0;
function Split(name, best, bestSplit) {
	best = best || NaN;
	bestSplit = bestSplit || NaN;
	
	this.ID = null;
	this.name = name;
	this.current = NaN;
	this.best = best;
	this.oldBest = best;
	this.bestSplit = bestSplit;
	this.oldBestSplit = bestSplit;
	this.parent = null;
	this.position = NaN;
	this.hash = 'split' + splitId++;
	
	this.elName = null;
	this.el = null;
	this.elBest = null;
	this.elSplit = null;
	this.btnDel = null;
	this.setStyle('old');
}
Split.prototype.setBest = function(t) {
	this.best = t;
	this.oldBest = t;
};
Split.prototype.setCurrent = function(t, updateBest) {
	var prevTime = this.position == 0 ? 0 : this.parent.splits[this.position - 1].current;
	var beatsTime = isNaN(this.best) || t < this.best;
	var beatsSplit = isNaN(this.bestSplit) || t - prevTime < this.bestSplit;
	this.current = t;
	
	if (beatsTime) {
		if (updateBest) this.best = t;
		if (beatsSplit) this.setStyle('btbs');
		else this.setStyle('bt');
	} else {
		if (beatsSplit) this.setStyle('bs');
		else this.setStyle('bad');
	}
	if (updateBest) {
		if (beatsTime) this.best = t;
		if (beatsSplit) this.bestSplit = t - prevTime;
	}
	
	this.update();
};
Split.prototype.reset = function() {
	this.current = NaN;
	
	if (this.el != null) this.el.text(fmtTime());
	this.setStyle('old');
};
Split.prototype.trash = function() {
	this.best = this.oldBest;
	this.bestSplit = this.oldBestSplit;
	this.update();
	this.reset();
};
Split.prototype.update = function() {
	if (this.el == null) return;
	this.el.text(fmtTime(this.current));
	this.elBest.text(fmtTime(this.best));
	this.elSplit.text(fmtTime(this.bestSplit));
};
Split.prototype.show = function() {
	var tr = $('<tr id="' + this.hash + '">');
	this.elName = $('<td class="left roboto">' + this.name + '</td>');
	this.el = $('<td class="' + this.style + '">' + fmtTime(this.current) + '</td>');
	this.elBest = $('<td>' + fmtTime(this.best) + '</td>');
	this.elSplit = $('<td>' + fmtTime(this.bestSplit) + '</td>');
	this.btnDel = $('<button class="control-edit ui-btn ui-icon-minus ui-btn-icon-notext ui-btn-inline ui-shadow"></button>');
	tr.append(this.elName);
	tr.append(this.el);
	tr.append(this.elBest);
	tr.append(this.elSplit);
	var td = $('<td>');
	td.append(this.btnDel);
	tr.append(td);
	var me = this;
	this.btnDel.click(function() { me.parent.remove(me); });
	return tr;
};
Split.prototype.setStyle = function(s) {
	this.style = s;
	if (this.el != null) this.el.attr('class', s);
};
Split.prototype.edit = function() {
	if (this.el == null) return;
	var me = this;
	$(this.elName).editable(function(v) {
		me.name = v;
		return v;
	});
	$(this.elBest).editable(function(v) {
		var t = parseTime(v);
		if (isNaN(t)) return fmtTime(me.best);
		me.best = t;
		me.oldBest = me.best;
		return fmtTime(t);
	});
	$(this.elSplit).editable(function(v) {
		var t = parseTime(v);
		if (isNaN(t)) return fmtTime(me.bestSplit);
		me.bestSplit = t;
		me.oldBestSplit = me.bestSplit;
		return fmtTime(t);
	});
};
Split.prototype.toJSON = function() {
	return {name: this.name, best: this.best, bestSplit: this.bestSplit, ID: this.ID, setID: this.parent.ID};
};
Split.fromJSON = function(data, parent) {
	var split = new Split(data.name, parseFloat(data.best), parseFloat(data.bestSplit));
	split.ID = data.ID;
	split.parent = parent;
	return split;
};
Split.prototype.saveBests = function() {
	this.oldBest = this.best;
	this.oldBestSplit = this.bestSplit;
};

/* LagDotSplits */
function LagDotSplits() {
	this.splits = null;
	this.active = false;
	this.timeTimeout = null;
	this.started = NaN;
	this.lastSplit = NaN;
	this.currentSplit = NaN;
	this.editing = false;
}
LagDotSplits.prototype.load = function(el, splits) {
	this.splits = splits;
	this.splits.parent = this;
	this.splits.show($(el));
	$('.control').enable();
};
LagDotSplits.prototype.start = function() {
	this.reset();
	this.active = true;
	this.started = getTime();
	this.lastSplit = 0;
	this.currentSplit = 0;
	this.movefocus();
	
	var lds = this;
	this.timeTimeout = window.setInterval(function() {
		var t = getTime() - lds.started;
		lds.splits.splits[lds.currentSplit].setCurrent(t, false);
	}, 10);
};
LagDotSplits.prototype.split = function() {
	var t = getTime() - this.started;
	this.splits.splits[this.currentSplit++].setCurrent(t, true);
	if (this.currentSplit == this.splits.splits.length) this.stop();
	this.lastSplit = t;
	this.movefocus();
};
LagDotSplits.prototype.stop = function() {
	this.active = false;
	window.clearInterval(this.timeTimeout);
	this.movefocus();
};
LagDotSplits.prototype.reset = function() {
	this.splits.reset();
};
LagDotSplits.prototype.trash = function() {
	this.splits.trash();
};
LagDotSplits.prototype.edit = function() {
	this.editing = true;
	this.splits.edit();
};
LagDotSplits.prototype.noedit = function() {
	this.editing = false;
	$('.editable').uneditable();
};
LagDotSplits.prototype.unsplit = function() {
	if (this.currentSplit > 0) {
		this.splits.splits[this.currentSplit].reset();
		this.currentSplit--;
		this.splits.splits[this.currentSplit].trash();
		this.movefocus();
	}
};
LagDotSplits.prototype.toJSON = function() {
	return this.splits.toJSON();
};
LagDotSplits.prototype.movefocus = function() {
	var hash = this.active ? this.splits.splits[this.currentSplit].hash : 'splitTable';
	$('html, body').animate({scrollTop: $('#' + hash).offset().top - $('#header').height()}, 500);
};

/* jQuery plugins */
$.fn.disable = function() { return this.attr('disabled', 'disabled'); }
$.fn.enable = function() { return this.removeAttr('disabled'); }
$.fn.editable = function(callback) {
	return this.each(function() {
		$(this).addClass('editable').click(function() {
			var v = window.prompt('Enter new value:', $(this).text());
			if (v == null) return;
			if (callback) v = callback(v);
			$(this).text(v);
		});
	});
};
$.fn.uneditable = function() {
	return this.each(function() {
		$(this).removeClass('editable').off('click');
	});
};

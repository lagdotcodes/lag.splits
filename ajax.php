<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once('db.lib.php');
require_once('password.php');

// Operations
function login($u, $p) {
	$username = dbEscape($u);
	$results = dbSearch('splitUser', "`name` = $username");
	$user = null;
	foreach ($results as $row) {
		if (check_password($p, $row['password'])) {
			$user = $row;
			break;
		}
	}
	
	if (!$user) {
		if (count($results)) return array('error' => 'Wrong password');
		else return array('error' => 'No such user');
	}
	
	$sets = dbSearch('splitSet', "`userID` = '{$user['ID']}'");
	foreach ($sets as &$set) $set['splitData'] = dbSearch('splitSplit', "`setID` = '{$set['ID']}'");
	return array('userID' => $user['ID'], 'sets' => $sets);
}

function register($u, $p) {
	if (!$u) return array('error' => 'Bad username');
	if (!$p) return array('error' => 'Bad password');

	$username = dbEscape($u);
	$results = dbSearch('splitUser', "`name` = $username");
	if (count($results)) return array('error' => 'User already exists');
	
	$password = make_password($p);
	if (!$password) die('Could not generate password hash');
	$id = dbSave('splitUser', array('name' => $u, 'password' => $password));
	return array('userID' => $id);
}

function save($set) {
	if (!$set) return array('error' => 'Bad set data');
	
	if ($set['ID']) {
		$id = dbEscape($set['ID']);
		$sql = "DELETE FROM splitSplit WHERE `setID`=$id";
		foreach ($set['splitData'] as $n => $split)
			if ($split['ID'])
				$sql .= " AND `ID`!=" . dbEscape($split['ID']);
		dbQuery($sql);
	}
	
	$splits = $set['splitData'];
	unset($set['splitData']);
	$id = dbSave('splitSet', $set);
	$splitids = array();
	foreach ($splits as $n => $split) {
		$split['setID'] = $id;
		$splitids[] = dbSave('splitSplit', $split);
	}
	
	return array('setID' => $id, 'splitIDs' => $splitids);
}

function run($setID, $userID, $splits, $times) {
	if (!$setID || !$userID || !$splits || !$times) return array('error' => 'Bad data');
	$id = dbSave('splitRun', array('setID' => $setID, 'userID' => $userID, 'splits' => $splits, 'times' => implode(',', $times)));
	$attempts = dbQuery("SELECT `ID` FROM splitRun WHERE `setID`=" . dbEscape($setID) . " AND `userID`=" . dbEscape($userID))->num_rows;
	return array('runID' => $id, 'attempts' => $attempts);
}

// Main
switch($_GET['op']) {
	case 'login':
		$u = array_key_exists('username', $_GET) ? $_GET['username'] : '';
		$p = array_key_exists('password', $_GET) ? $_GET['password'] : '';
		$out = login($u, $p);
		break;
	
	case 'register':
		$u = array_key_exists('username', $_GET) ? $_GET['username'] : '';
		$p = array_key_exists('password', $_GET) ? $_GET['password'] : '';
		$out = register($u, $p);
		break;
	
	case 'save':
		$set = array_key_exists('set', $_GET) ? $_GET['set'] : null;
		$out = save($set);
		break;
	
	case 'run':
		$setID = array_key_exists('setID', $_GET) ? $_GET['setID'] : null;
		$userID = array_key_exists('userID', $_GET) ? $_GET['userID'] : null;
		$splits = array_key_exists('splits', $_GET) ? $_GET['splits'] : 0;
		$times = array_key_exists('times', $_GET) ? $_GET['times'] : null;
		$out = run($setID, $userID, $splits, $times);
		break;
	
	default:
		$out = array('error' => 'Invalid operation');
}

header('Content-type: application/json');
echo json_encode($out);

<?php
require_once('config.php');

function dbQuery($sql) {
	global $DB;
	$result = $DB->query($sql);
	if (!$result) die("query[$sql]: " . $DB->error);
	return $result;
}

function dbEscape($value) {
	global $DB;
	if ($value === null || $value == 'NaN') return 'NULL';
	return "'" . $DB->escape_string($value) . "'";
}

function dbSave($table, $fields) {
	global $DB;
	if (array_key_exists('ID', $fields) && $fields['ID']) {
		$id = $fields['ID'];
		$assignments = array();
		foreach ($fields as $k => $v) {
			if ($k == 'ID') continue;
			$fv = dbEscape($v);
			$assignments[] = "`$k`=$fv";
		}
		$assignments = implode(', ', $assignments);
		$sql = "UPDATE `$table` SET $assignments WHERE `ID`='$id'";
		dbQuery($sql);
		return $id;
	} else {
		$keys = array();
		$values = array();
		foreach ($fields as $k => $v) {
			if ($k == 'ID') continue;
			$keys[] = "`$k`";
			$values[] = dbEscape($v);
		}
		$keys = implode(', ', $keys);
		$values = implode(', ', $values);
		$sql = "INSERT INTO `$table` ($keys) VALUES ($values)";
		dbQuery($sql);
		return $DB->insert_id;
	}
}

function dbGet($table, $id) {
	$results = dbSearch($table, "`ID`='$id'");
	return count($results) ? $results[0] : null;
}

function dbSearch($table, $filter) {
	global $DB;
	$sql = "SELECT * FROM `$table` WHERE $filter";
	$set = dbQuery($sql);
	$results = array();
	while ($row = $set->fetch_assoc()) {
		$ssr = array();
		foreach ($row as $k => $v) {
			if (is_string($v)) $v = stripslashes($v);
			$ssr[$k] = $v;
		}
		$results[] = $ssr;
	}
	return $results;
}

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
	<title>Lag.Splits</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.3/jquery.mobile.min.css" />
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Droid+Sans+Mono|Roboto:400,700" />
	<link rel="stylesheet" href="splits.css" />
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.3/jquery.mobile.min.js"></script>
	<script src="splits.lib.js"></script>
	<script src="splits.js"></script>
</head>
<body class="roboto">
<div data-role="page">
	<div id="header" data-role="header" data-position="fixed" data-tap-toggle="false">
		Lag.Splits v0.3b
	</div>
	<div role="main" class="ui-content">
		<div id="loginform">
			<label for="username" class="ui-hidden-accessible">Username:</label>
			<input type="text" id="username" placeholder="Username" />
			<label for="password" class="ui-hidden-accessible">Password:</label>
			<input type="password" id="password" placeholder="Password" />
			<div data-role="controlgroup" data-type="horizontal" data-mini="true">
				<button class="ui-shadow ui-btn ui-corner-all" id="login">Login</button>
				<button class="ui-shadow ui-btn ui-corner-all" id="register">Register</button>
			</div>
		</div>
		<div id="setform" style="display: none">
			<label for="loadset" class="select ui-hidden-accessible">Sets</label>
			<select id="loadset"></select>
			
			<span>New Splits Set:</span>
			<label for="game" class="ui-hidden-accessible">Game:</label>
			<input type="text" id="game" placeholder="Game" />
			<label for="category" class="ui-hidden-accessible">Category:</label>
			<input type="text" id="category" placeholder="Category" />
			<button class="ui-shadow ui-btn ui-corner-all" id="create">Create</button>
		</div>
		<div id="splits" style="display: none"></div>
	</div>
	<div data-role="footer" data-position="fixed" data-tap-toggle="false">
		<button id="split"   class="control control-noedit ui-btn ui-btn-inline ui-btn-icon-notext ui-shadow ui-icon-carat-r" title="Split" disabled></button>
		<button id="unsplit" class="control control-noedit ui-btn ui-btn-inline ui-btn-icon-notext ui-shadow ui-icon-carat-l" title="Unsplit" disabled></button>
		<button id="reset"   class="control control-noedit ui-btn ui-btn-inline ui-btn-icon-notext ui-shadow ui-icon-delete" title="Reset" disabled></button>
		<button id="trash"   class="control control-noedit ui-btn ui-btn-inline ui-btn-icon-notext ui-shadow ui-icon-forbidden" title="Discard New Bests" disabled></button>
		<button id="edit"    class="control control-noedit ui-btn ui-btn-inline ui-btn-icon-notext ui-shadow ui-icon-edit" title="Edit" disabled></button>
		
		<button id="noedit"  class="control control-edit ui-btn ui-btn-inline ui-btn-icon-notext ui-shadow ui-icon-check" title="Done Editing" disabled></button>
		<button id="add"     class="control control-edit ui-btn ui-btn-inline ui-btn-icon-notext ui-shadow ui-icon-plus" title="Add Split" disabled></button>
		<button id="save"    class="control control-edit ui-btn ui-btn-inline ui-btn-icon-notext ui-shadow ui-icon-action" title="Save Set" disabled></button>
	</div>
</div>
</body>
</html>

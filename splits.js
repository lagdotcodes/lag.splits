S = new LagDotSplits();

function demo() {
	var set = new Splits('Testing', 'any%');
	set.add(new Split('Break OOB', 5));
	set.add(new Split('Kill 3rd Boss', 25));
	set.add(new Split('Kill Last Boss', 70));
	showSplits(set);
}

function showSets(sets) {
	if (sets.length > 0) {
		$('#loadset-button').show();
		$('#loadset-button span').text('Select a splits set');
		$('#loadset').empty().append('<option value="" selected></option>');
		for (var i = 0; i < sets.length; i++) {
			var set = sets[i];
			$('#loadset').append('<option value="' + set.ID + '">' + set.game + ' (' + set.category + ')</option>');
		}
	} else {
		$('#loadset-button').hide();
	}
	$('#loginform').hide();
	$('#setform').show();
}

function showSplits(set) {
	S.load('#splits', set);
	$('#setform').hide();
	$('#splits').show();
	$('.control-edit').hide();
}

function saveRun() {
	var set = S.splits;
	if (!set.ID) return;
	
	var t = [];
	set.splits.forEach(function(split) { t.push(split.current); });
	ajaxOp('run', {setID: set.ID, userID: S.userID, splits: set.splits.length, times: t});
}

$(function() {
	$('#login').click(function() {
		ajaxOp('login', {username: $('#username').val(), password: $('#password').val()}, function(d) {
			S.userID = d.userID;
			showSets(d.sets);
			S.loadedSets = d.sets;
		});
	});

	$('#register').click(function() {
		ajaxOp('register', {username: $('#username').val(), password: $('#password').val()}, function(d) {
			S.userID = d.userID;
			showSets([]);
		});
	});
	
	$('#loadset').change(function() {
		var setID = $(this).val();
		for (var i = 0; i < S.loadedSets.length; i++) {
			if (S.loadedSets[i].ID == setID) {
				showSplits(Splits.fromJSON(S.loadedSets[i]));
				break;
			}
		}
	});
	
	$('#create').click(function() {
		var game = $('#game').val();
		var cat = $('#category').val();
		if (game && cat) {
			var set = new Splits(game, cat);
			set.userID = S.userID;
			showSplits(set);
		}
	});
	
	$('#split').click(function() {
		if (S.splits == null || S.splits.splits.length == 0) return;
	
		if (S.active) S.split();
		else S.start('#timer');
		
		if (S.active)$('#edit').disable();
		else {
			saveRun();
			$('#edit').enable();
		}
	});
	
	$('#unsplit').click(function() {
		if (S.active) S.unsplit();
	});
	
	$('#reset').click(function() {
		if (S.active) {
			saveRun();
			S.stop();
		}
		S.reset();
		$('#edit').enable();
	});
	
	$('#trash').click(function() {
		if (S.active) {
			saveRun();
			S.stop();
		}
		S.trash();
		$('#edit').enable();
	});
	
	$('#noedit').click(function() {
		S.noedit();
		$('.control-noedit').show();
		$('.control-edit').hide();
	});
	
	$('#edit').click(function() {
		S.edit();
		$('.control-noedit').hide();
		$('.control-edit').show();
	});
	
	$('#add').click(function() {
		var name = window.prompt('Split name:', '');
		if (name == null) return;
		var s = new Split(name);
		s.parent = S.splits;
		S.splits.add(s);
		S.load('#splits', S.splits);
		S.edit();
	});
	
	$('#save').click(function() {
		S.splits.saveBests();
		ajaxOp('save', {set: S.splits.toJSON()}, function(d) {
			S.splits.ID = d.setID;
			for (var i = 0; i < d.splitIDs.length; i++) S.splits.splits[i].ID = d.splitIDs[i];
		});
	});

	$('.control-edit').hide();
});

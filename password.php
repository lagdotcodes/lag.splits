<?php
function make_password($p) {
	if (function_exists('password_hash'))
		return password_hash($p, PASSWORD_DEFAULT);
	
	if (function_exists('crypt')) {
		$salt = str_replace('+', '.', substr(base64_encode(mcrypt_create_iv(17)), 0, 22));
		$parm = '$' . implode('$', array('2y', '10', $salt));
		return crypt($p, $parm);
	}
	
	die('make_password: no method');
}

function check_password($p, $hash) {
	if (function_exists('password_verify'))
		return password_verify($p, $hash);	
	
	if (function_exists('crypt'))
		return crypt($p, $hash) == $hash;
	
	die('check_password: no method');
}
